---
title: Partial Mocks
---
h2. Partial Mocks

RSpec allows you <a href="stubs.html">stub methods</a> and set <a href="mocks.html">mock
expectations</a> for methods on existing Class objects. The main reason for including
this is to support mocking/stubbing Ruby on Rails model class methods in controller and/or view specs.

<ruby>
MyModel.should_receive(:find).with(id).and_return(@mock_model_instance)
</ruby>

<a href="mocks.html">Mocking</a> (and/or <a href="stubs.html">stubbing</a>) the class
level methods and having them return a mock instead of a real instance
of the model class allows you to spec your controllers and views in isolation from the
instance level logic of your model classes. This means that you can change the validation
rules for a model, for example, and drive that in the model specs without affecting the
controller and view specs.

This also helps to keep the context of your spec completely in view (no having to look at fixtures/xyz.yml to understand what's going on).

Additionally, although we haven't completely isolated specs from the
database yet (you still need one for the models to actually work), this sort of mocking
will save you trips to the database, speeding things up quite a bit in a larger app.