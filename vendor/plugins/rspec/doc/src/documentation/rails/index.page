---
title: Spec::Rails 
---
h2. Spec::Rails - Overview

A Rails plugin that brings RSpec to Rails.

h3. Features

* Use RSpec to independently specify models, views, controllers and helpers.
* Integrated fixture loading.
* Special generators for models and controllers that generate specs instead of tests.
* Special RSpec matchers for even more readable specs.

h3. Vision

For people for whom TDD is a brand new concept, the testing support built into Rails is a huge
leap forward. The fact that it's built right in is fantastic, and Rails
apps are generally much easier to maintain than they would have been without such support.

For those of us coming from a history with TDD, and now BDD, the existing support
presents some problems related to dependencies across specs. To that end, RSpec
supports 4 types of specs. This is largely inspired by Test::Rails, which is
the rails testing framework built into "ZenTest":http://zentest.rubyforge.org. 
We've also built in first class mocking
and stubbing support in order to break dependencies across these different concerns.

h2. Different Contexts for different Contexts

Spec::Rails supports different Context subclasses for the following types of specs:

h3. Model Examples

These are the equivalent of unit tests in Rails'' built in testing. Ironically
(for the traditional TDD'er) these are the only specs that we feel should actually interact
with the database. "Learn more...":writing/models.html.

h3. Controller Examples

These align somewhat with functional tests in rails, except
that they do not actually render views (though you can force rendering of views
if you prefer). Instead of setting expectations about what goes on a page, you
set expectations about what templates get rendered. "Learn more...":writing/controllers.html.

h3. View Examples

This is the other half of Rails'' functional testing. View specs allow
you to set up assigns (thanks to ZenTest). "Learn more...":writing/views.html.

h3. Helper Examples

... let you specify directly methods that live in your helpers. "Learn more...":writing/helpers.html.

h3. Integration Examples

We also plan to support "Integration Examples":writing/integration.html in the future.

For the time being we recommend that you use either
"rails integration testing":http://weblog.jamisbuck.org/2006/3/9/integration-testing-in-rails-1-1
 or another framework like "watir":http://wtr.rubyforge.org/ or "selenium":http://www.openqa.org/selenium/.

h2. Fixtures

You can use fixture in any of your specs, be they model, view, controller, or helper. If you have fixtures that you want to use
globally, you can set them in spec/spec_helper.rb. See that file for more information. 

h2. Naming conventions

For clarity and consistency, RSpec uses a slightly different naming convention
for directories and Rake tasks than what you get from the Test::Unit testing built into rails.

<ruby>
project
  |
  +--app
  |
  +--components
  |
  +--...
  |
  +--spec
      |
      +-- spec_helper.rb
      |
      +-- controllers
      |
      +-- helpers
      |
      +-- models
      |
      +-- views
</ruby>

The Rake tasks are named accordingly.

